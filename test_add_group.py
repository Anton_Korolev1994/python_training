# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestAdressbook():

    def setup_method(self, method):
        self.driver = webdriver.Firefox()
        self.vars = {}

    def test_adressbook(self):
        wd = self.driver
        self.open_home_page(wd)
        self.login(wd)
        self.open_group_page(wd)
        self.create_group(wd)
        self.return_to_group_page(wd)
        self.logout(wd)

    def logout(self, wd):
      wd.find_element(By.LINK_TEXT, "Logout").click()

    def return_to_group_page(self, wd):
      wd.find_element(By.LINK_TEXT, "group page").click()

    def create_group(self, wd):
      # init group creation
      wd.find_element(By.NAME, "new").click()
      # fill group form
      wd.find_element(By.NAME, "group_header").click()
      wd.find_element(By.NAME, "group_name").click()
      wd.find_element(By.NAME, "group_name").send_keys("dfsfsdf")
      wd.find_element(By.NAME, "group_header").click()
      wd.find_element(By.NAME, "group_header").send_keys("sdfsdfsdf")
      wd.find_element(By.NAME, "group_footer").click()
      wd.find_element(By.NAME, "group_footer").send_keys("sdfsdfsdf")
      # submit group creation
      wd.find_element(By.NAME, "submit").click()

    def open_group_page(self, wd):
      wd.find_element(By.LINK_TEXT, "groups").click()

    def login(self, wd):
        wd.find_element(By.NAME, "user").send_keys("admin")
        wd.find_element(By.NAME, "pass").send_keys("secret")
        wd.find_element(By.CSS_SELECTOR, "input:nth-child(7)").click()

    def open_home_page(self, wd):
        wd.get("http://addressbook/")
        wd.maximize_window()

    def teardown_method(self, method):
        self.driver.quit()
